function ListShoes(props) {
  const deleteShoe = async(shoe) => {
    const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}/`;
    const fetchConfig = {
      method: "delete",
    }
    const response = await fetch(shoeUrl, fetchConfig)
    if (response.ok) {
      props.getShoes()
    }
  }






      return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Href</th>
          </tr>
        </thead>
        <tbody>
        {props.shoes.map(shoe => {
          console.log(shoe)
          return (
            <tr key={shoe.href}>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.color }</td>
              <td>{ shoe.bin }</td>
              <td><button onClick={() => deleteShoe(shoe)}>Delete</button></td>
            </tr>
          );
        })}
        </tbody>
      </table>
    )
}

export default ListShoes
