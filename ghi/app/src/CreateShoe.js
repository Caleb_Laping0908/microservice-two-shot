import React, {useEffect, useState} from 'react';

function ShoeForm(props) {


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = picture;
        data.bin = bin;

        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
          const newShoe = await response.json();
          console.log(newShoe);
          setManufacturer('');
          setModelName('');
          setColor('');
          setPicture('');
          setBin('');
          props.getShoes();
        }
    }

  const [manufacturer, setManufacturer] = useState('');
  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const [modelName, setModelName] = useState('');
  const handleModelNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  }

  const [color, setColor] = useState('');
  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const [picture, setPicture] = useState('');
  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  }

  const [bin, setBin] = useState('');
  const handleBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  }

  const [bins, setBins] = useState([]);
  const fetchData = async () => {

    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins)
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Add a new shoe!</h1>
        <form onSubmit={handleSubmit} id="create-shoe-form">
          <div className="form-floating mb-3">
            <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
            <label htmlFor="manufacturer">Manufacturer</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
            <label htmlFor="model_name">Model Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
            <label htmlFor="color">Color</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handlePictureChange} value={picture} placeholder="Picture URL" required type="text" name="picture" id="picture" className="form-control" />
            <label htmlFor="picture">Picture URL</label>
          </div>
          <div className="mb-3">
            <select onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                <option value="">Choose a Wardrobe</option>
                {bins.map(bin => {
                    return (
                        <option key={bin.href} value={bin.href}>
                        {bin.closet_name}
                        </option>
                    );
                })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}




export default ShoeForm;
