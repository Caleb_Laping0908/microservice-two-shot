from django.contrib import admin
from .models import Hats, LocationVO

@admin.register(Hats)
class HatAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "style_name",
        "location",
    )

@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "import_href",
        "closet_name",
    )
