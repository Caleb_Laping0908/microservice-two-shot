from django.shortcuts import render
from .models import Shoe
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "closet_name",
        "import_href",
        "bin_number",
        "bin_size",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.import_href}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        print("HERE_CONTENT", content)

        try:
            bin_href = content["bin"]
            print("HERE_HREF", bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            print("bin", bin)
            content["bin"] = bin
            print("HERE_CONTENT_LATER", content)
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin href"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        print("HERE_SHOE", shoe)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe, encoder=ShoeDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "bin" in content:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"}, status=400
            )
        Shoe.objects.filter(id=id).update(**content)
        attendee = Shoe.objects.get(id=id)
        return JsonResponse(
            attendee, encoder=ShoeDetailEncoder, safe=False
        )
