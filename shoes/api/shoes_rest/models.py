from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, null=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)


    def __str__(self):
        return f"{self.import_href}"


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200, null=True)
    model_name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    picture_url = models.URLField(null=True)


    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        null=True,
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("api_list_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return self.model_name
