# Generated by Django 4.0.3 on 2023-01-20 17:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0007_remove_shoe_wardrobe_bin'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='wardrobe_bin',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
